const jwt = require('jsonwebtoken');
const { config } = require('dotenv')
const User = require('../modules/user/models/user.model');
config();

module.exports.authentication = (req, res, next) => { 
  try {
    const access_token = req.headers.access_token; 
    if (!access_token) { 
      throw {name: "MISSING_TOKEN"};
    }
    
    const key = process.env.JWT_KEY;
    jwt.verify(access_token, key, (err, decoded) => {
      if (err) throw {name: "INVALID_TOKEN"};
      
      req.userId = decoded.userId;
      next();
    });
  } catch (err) {
    next(err);
  }
};