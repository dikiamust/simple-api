module.exports = (err, req, res, next) => {
  let code;
  let name = err.name;
  let message;

  switch (name) {
    case 'REQUIRED':
      code = 400;
      message = 'Missing required field.';
      break;

    case 'FALSE_PASS':
      code = 400;
      message = 'Passwords do not match!';
      break;

    case 'CITY_NOT_FOUND':
      code = 404;
      message = 'City is not found or has been deleted.';
      break;

    case 'PROFILE_NOT_FOUND':
      code = 404;
      message = 'Profile is not found.';
      break;

    case 'USER_NOT_FOUND':
      code = 404;
      message = 'User is not found or has been deleted.';
      break;

    case 'CANT_DELETE_CITY':
      code = 403;
      message = 'There are profiles using this city.';
      break;

    case 'FALSE_PASSWORD':
      code = 403;
      message = 'The password you provided is incorrect.';
      break;

    case 'FALSE_NEWPASSWORD':
      code = 403;
      message = 'The new password does not match the new password confirmation.';
      break;
    
    case 'FALSE_LOGIN':
      code = 401;
      message = 'The email or password you provided is incorrect. Please double-check your login credentials and try again.';
      break;
    
    case 'INVALID_TOKEN':
      code = 401;
      message = 'The access token is invalid or has expired. Please check your access token and try again.';
      break;

    case 'MISSING_TOKEN':
      code = 401;
      message = 'Access to this resource requires authentication. Please log in to your account to access this resource.';
      break;
    
    case 'INVALID_PASSWORD':
      code = 400;
      message = 'Password must contain at least one uppercase letter, one lowercase letter, one number, one special character & the length minimum is 8';
      break;

    case 'INVALID_EMAIL':
      code = 400;
      message = 'The email you provided is invalid formatted.';
      break;

    case 'FORBIDDEN':
      code = 403;
      message = 'Forbidden access.';
      break;

    default:
      code = 500;
      message = 'Internal Server Error: Sorry, something went wrong on our end. Our team has been notified and we are working to fix the issue as soon as possible. Please try again later.';
  }

  if( err.code && err.code == 11000) {
    return  res.status(400).json({success: false, message: err.toString()});
  }
 
  if( err.name && err.name === 'ValidationError') {
    return  res.status(400).json({success: false, message: err.toString()});
  }

  res.status(code).json({success: false, message});
};
