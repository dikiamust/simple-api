const mongoose = require('mongoose');
const { config } = require('dotenv');
config();

const connectDB = async () => {
  try {
    await mongoose.connect(process.env.DB_HOST, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    const env = process.env.NODE_ENV;
    console.log(`Database ${env} (MongoDB) connected successfully!`);
  } catch (error) {
    console.error('Failed to connect to MongoDB', error);
    process.exit(1);
  }
};

module.exports = connectDB;

