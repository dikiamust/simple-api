const { createLogger, transports, format } = require('winston');
const { config } = require('dotenv');

config();

const customFormat = format.combine(
  format.timestamp(),
  format.printf((info) => {
    const message = info[Symbol.for('splat')]
      ? util.format(info.message, ...info[Symbol.for('splat')])
      : info.message;
    return `${info.timestamp} [${info.level.toUpperCase().padEnd(7)}] : ${message}`;
  })
);


const destinations = [new transports.Console()];
destinations.push(new transports.File({ filename: `${process.env.NODE_ENV}.log` }));


const logger = createLogger({
  transports: destinations,
  level: 'debug',
  format: customFormat,
  // in the test environment, the logger will be silent
  silent: process.env.NODE_ENV === 'test',
});

module.exports = logger;