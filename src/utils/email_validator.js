const emailValidator = (email) => {
  // Regular expression for email validation
  const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
  const isValid = emailRegex.test(email)
  if (!isValid) throw { name: 'INVALID_EMAIL' };  

  return email;
};

module.exports =  { emailValidator }; 
