const logger = require('./logger_config');
const textColor  = require('./text_color');

const loggerFunct = (req, res) => {
  logger.info(`Request: ${req.method} ${req.url}`);

  res.on('finish', () => {
    if(res.statusCode === 200 || res.statusCode === 201 ){
      logger.info(textColor.green + (`Response: status code : ${res.statusCode}` + textColor.reset));
    }else{
      logger.info(textColor.red + (`Response: status code : ${res.statusCode}` + textColor.reset));
    }
  });
}

module.exports = loggerFunct;