const passwordValidator = (password) => {
  const uppercaseChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const lowercaseChars = 'abcdefghijklmnopqrstuvwxyz';
  const numberChars = '0123456789';
  const uniqueChars = '!@#$%^&*()_+-=[]{}|;:,.<>?';

  // Check for at least one character from each character set
  const hasUppercase = [...password].some((char) => uppercaseChars.includes(char));
  const hasLowercase = [...password].some((char) => lowercaseChars.includes(char));
  const hasNumber = [...password].some((char) => numberChars.includes(char));
  const hasUnique = [...password].some((char) => uniqueChars.includes(char));

  const passLength = 8;
  const isValid = hasUppercase && hasLowercase && hasNumber && hasUnique && password.length >= passLength;
    
  if (!isValid)  throw { name: 'INVALID_PASSWORD' };  

  return password;
};

module.exports =  { passwordValidator }; 
