const User = require('../models/user.model');

const findOneUserById = async (userId) => {
  const findUser = await User.findOne({ _id: userId, is_deleted: false })
    .select('-password -is_deleted -__v')
    .populate({
      path: 'profile_id',
      select: '-is_deleted -created_at -updated_at -__v',
    })
    
  if (!findUser) throw { name: 'USER_NOT_FOUND' };  
  
  return findUser;
}

module.exports = { findOneUserById };