const User = require('../models/user.model');


module.exports.getAll = async (req, res, next) => {
  try {
    const page = parseInt(req.query.page) || 1; 
    const perPage = parseInt(req.query.perPage) || 10; 
    const skip = (page - 1) * perPage;

    const users = await User.find({ is_deleted: false })
      .select('-password -is_deleted -__v')
      .populate({
        path: 'profile_id',
        select: '-is_deleted -created_at -updated_at -__v',
      })
      .skip(skip)
      .limit(perPage);

    res.status(200).json(users);
  } catch (error) {
    next(error);
  }
};