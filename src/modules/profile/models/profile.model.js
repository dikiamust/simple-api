const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const profileSchema = new Schema(
  {
    name: {
      type: String,
      required: [true, 'Required name'],
      unique: true
    },
    address: {
      type: String,
      required: [true, 'Required address'],
    },
    city_id: [{ 
      type: mongoose.Types.ObjectId, 
      ref: 'City', 
      required: true 
    }],
    hobbies: [{
      type: String
    }],
    is_deleted: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  }
);

// Populate 'city_id' field
profileSchema
  .pre('findOne', function (next) {
    this.populate({
      path: 'city_id',
      select: 'name _id', 
    });
    next();
})
  .pre('find', function (next) {
    this.populate({
      path: 'city_id',
      select: 'name _id',
    });
    next();
});


const Profile = mongoose.model('Profile', profileSchema);
module.exports =  Profile;

