const Profile = require('../models/profile.model');

const findOneProfileByCityId = async (cityId) => {
  const findProfile = await  Profile.findOne({ city_id: cityId });
  if (!findProfile) throw { name: 'PROFILE_NOT_FOUND' };  
  
  return findProfile;
}

module.exports = { findOneProfileByCityId };