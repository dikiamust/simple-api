const express = require('express');
const indexRouter = express.Router();
const authRouter = require('./auth/routes/auth.routes');
const cityRouter = require('./city/routes/city.routes');
const userRouter = require('./user/routes/user.routes')


indexRouter.get('/', (req, res) => {
  res.status(200).send('Simple API is running!');
});

indexRouter.use('/api/v1/auth', authRouter)
indexRouter.use('/api/v1/city', cityRouter);
indexRouter.use('/api/v1/user', userRouter);

module.exports = indexRouter;
