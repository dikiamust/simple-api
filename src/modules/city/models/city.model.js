const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const citySchema = new Schema(
  {
    name: {
      type: String,
      required: [true, 'Required name'],
      unique: true
    },
    is_deleted: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  }
);

const City = mongoose.model('City', citySchema);
module.exports =  City;

