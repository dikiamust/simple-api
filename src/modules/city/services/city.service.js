const City = require('../models/city.model');

const findOneCityById = async (cityId) => {
  if(!cityId)throw { name: 'REQUIRED' };  

  const findCity = await City.findById(cityId).where({ is_deleted: false });
  if (!findCity) throw { name: 'CITY_NOT_FOUND' };  
  
  return findCity;
}

module.exports = { findOneCityById };