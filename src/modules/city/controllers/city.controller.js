const City = require('../models/city.model');
const { findOneCityById } = require('../services/city.service')
const { findOneProfileByCityId } = require('../../profile/service/profile.service')

module.exports.createCity = async (req, res, next) => {
  try {
    const payload = req.body;
    const { name } = payload;
    const item = await City.create(payload);
  
    res.status(201).json({
      message: 'City was successfully added'
    });
  } catch (error) {
    next(error);
  }
};

module.exports.getAllCities = async (req, res, next) => {
  try {
    const page = parseInt(req.query.page) || 1; 
    const perPage = parseInt(req.query.perPage) || 10; 
    const skip = (page - 1) * perPage;

    const city = await City.find().where({is_deleted: false})
      .select('name')
      .skip(skip)
      .limit(perPage);
   
    res.status(200).json(city);
  } catch (error) {
    next(error);
  }
};

module.exports.getCityById = async (req, res, next) => {
  const { cityId } = req.params;

  try {
    const city = await findOneCityById(cityId);
    
    res.status(200).json({
      message: 'City details successfully retrieved.',
      data: city
    });
  } catch (error) {
    next(error);
  }
};

module.exports.updateCity = async (req, res, next) => {
  const { cityId } = req.params;
  const payloadBody = req.body;
  const { name } = payloadBody;

  try {
    if (!name) throw {name: 'REQUIRED'};
    await findOneCityById(cityId);
    await City.updateOne({ _id: cityId }, payloadBody);

    res.status(200).json({
      message: 'City was successfully updated',
    });    
  } catch (error) {
    next(error);
  }
};

module.exports.deleteCity = async (req, res, next) => {
  const { cityId } = req.params;
 
  try {
    await findOneCityById(cityId);

    const cityInProfile = findOneProfileByCityId(cityId)
    if(cityInProfile) throw {name: 'CANT_DELETE_CITY'};

    await City.updateOne({ _id: cityId }, { is_deleted: true});

    res.status(200).json({
      message: 'City was successfully deleted',
    });
  } catch (error) {
    next(error);
  }
};

