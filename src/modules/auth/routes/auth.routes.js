const express = require('express');
const authRouter = express.Router();
const authController = require('../controllers/auth.controller');
const authMiddlewares = require('../../../middlewares/authMiddlewares');

authRouter.post('/signup', authController.signup);
authRouter.post('/signin', authController.signin);

/**
 * These endpoints are protected and require user authentication.
 * Authentication ensures that only authorized users can perform actions here.
 */
authRouter.use(authMiddlewares.authentication);
authRouter.get('/account', authController.getDetailAccount);
authRouter.delete('/account', authController.deleteAccount);
authRouter.put('/change-password', authController.changePassword);

module.exports =  authRouter;
