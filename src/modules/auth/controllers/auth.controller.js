const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../../user/models/user.model');
const Profile = require('../../profile/models/profile.model');
const { findOneCityById } = require('../../city/services/city.service');
const { findOneUserById } = require('../../user/services/user.service');
const { passwordValidator } = require('../../../utils/password_validator');
const { emailValidator } = require('../../../utils/email_validator');
const { config } = require('dotenv');
config();

module.exports.signup = async (req, res, next) => {
  let profileCreted = undefined;
  try {
    const { email, password, confirmPassword, name, address, cityId, hobbies} = req.body;
    emailValidator(email)
    passwordValidator(password)
    if(password !== confirmPassword) throw {name: 'FALSE_PASS'};

    const city = await findOneCityById(cityId);
    const profile = await Profile.create({
      name,
      address,
      city_id: city.id,
      hobbies,
    });

    profileCreted = profile;

    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(password, salt);
    const user = await User.create({
      email,
      password: hash,
      profile_id: profile.id,
    });

    if(user){
      res.status(201).json({
        message: 'User was registered successfully',
      });
    }
    return;
  } catch (err) {
    if(profileCreted !== undefined) {
      Profile.deleteOne({_id: profileCreted._id})
    }
    next(err);
  }
};

module.exports.signin =  async (req, res, next) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (!user) throw {name: 'FALSE_LOGIN'};

    const isMatch = bcrypt.compareSync(
      password,
      user.password
    );

    if (!isMatch) throw {name: 'FALSE_LOGIN'};
    
    const key = process.env.JWT_KEY;
    const expiresIn = process.env.JWT_EXPIRE_IN;
    const token = jwt.sign({userId: user.id }, key, {
      expiresIn,
    });

    await User.updateOne({ _id: user._id }, { last_login: Date.now() });

    res.status(200).json({
      message: 'Login successful!',
      data: {
        accessToken: token,
      }
    });
    
   } catch (error) {
    next(error);
  }
};

module.exports.getDetailAccount = async (req, res, next) => {
  try {
    const { userId } = req.userId;
    const user = await findOneUserById(userId)

    res.status(200).json(user);
  } catch (error) {
    next(error);
  }
};

module.exports.deleteAccount = async (req, res, next) => {
  try {
    const { userId } = req.userId;
    const user = await findOneUserById(userId)

    await User.updateOne({ _id: user._id }, { is_deleted : true });
    await Profile.updateOne({ _id: user.profile_id }, { is_deleted : true });

    res.status(201).json({
      message: 'User was successfully deleted',
    });
  } catch (error) {
    next(error);
  }
};

module.exports.changePassword = async (req, res, next) => {
  try {
    const { userId } = req.userId;
    const { currentPassword, newPassword, confirm_newPassword } = req.body;
   
    const user = await User.findOne({ _id: userId  });
    if (!user) throw { name: 'USER_NOT_FOUND' };  

    const isMatch = bcrypt.compareSync(
      currentPassword,
      user.password
    );

    if (!isMatch) throw {name: 'FALSE_PASSWORD'};

    passwordValidator(newPassword)
    if(newPassword !== confirm_newPassword){
      throw {name: 'FALSE_NEWPASSWORD'};
    }

    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(newPassword, salt);

    await User.updateOne({ _id: user._id }, { password : hash });

    res.status(201).json({
      message: 'Password was successfully updated',
    });
  } catch (error) {
    next(error);
  }
};

