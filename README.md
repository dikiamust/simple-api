## Getting started

Follow these steps to set up The Project on your machine:

1. Create a .env file.<br />
2. Copy the .env.example file and rename it to .env.<br />
3. Update the environment variables in this file to match your local setup.<br />
4. Install All Dependencies by this command:

```sh
$ yarn install
```

---

5. Run the Project: Use one of the following commands to run the project locally:

yarn dev: This command will start the development server with hot reloading enabled.<br />
yarn start: This command will start the production server.<br />

## Description

This is a simple API built with ExpressJs & MongoDB<br />

The Postman collection is provided and can be found in the 'postman-collection' directory.<br />
The exported Database is provided and can be found in the 'simple-api-db' directory.
